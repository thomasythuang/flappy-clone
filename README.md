A simple clone of Flappy Bird written during a Learning Day for [Yello's](https://yello.co/) engineering department

This game was written in Lua with the free and open source game engine [Love2D](https://love2d.org/). Feel free to fork and pull it down to play around with both the game and code!

### Instructions:
Follow these steps to set up a local development environment for love: http://sheepolution.com/learn/book/1

The rest of that tutorial is also a great guide if you're getting started with either love, Lua, or game development in general

### Game Distribution:
To package the game for distribution, follow the approriate steps for your given OS: https://love2d.org/wiki/Game_Distribution