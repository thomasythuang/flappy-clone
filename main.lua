function love.load()
  Object = require 'classic'
  require 'constants'
  require 'block'
  require 'wall'
  require 'player'
  
  initNewGame()
end

function love.update(dt)
  if not gameOver then
    updateWalls(dt)
    player:update(dt)
    checkCollisions()
  end
end

function love.draw()
  player:draw()
  drawWalls()
  drawText()
end

function initNewGame()
  gameOver = false
  score = 0
  walls = {}
  player = Player()

  table.insert(walls, Wall())
  love.graphics.setBackgroundColor({248, 239, 186})
end

function love.keypressed(key)
  if key == 'space' then
    player:jump()
  end
  
  if key == 'return' and gameOver then
    initNewGame()
  end
end

function checkCollisions()
  if (walls[1]:checkCollision(player)) or
     (player.y > WINDOW_HEIGHT - Player.size / 2) then
    gameOver = true
  end
end

function updateWalls(dt)
  -- generate new wall
  if (WINDOW_WIDTH - Wall.width - walls[#walls].x) > Wall.xInterval then
    table.insert(walls, Wall())
  end
  
  -- remove/garbage collect walls when they're off screen, add to score
  if (walls[1].x + Wall.width < 0) then
    table.remove(walls, 1)
    score = score + 1
  end

  for _,wall in ipairs(walls) do
    wall:update(dt)
  end
end

function drawWalls()
  for _,wall in ipairs(walls) do
    wall:draw()
  end
end

function drawText()
  love.graphics.setColor({10, 10, 10})
  love.graphics.print('Score: ' .. score, 10, 10)
  
  if gameOver then
    love.graphics.print('Game Over! \nPress Enter to restart', 10, 25)
  end
end

