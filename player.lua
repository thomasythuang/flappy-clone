Player = Object:extend()

Player.size = 50

local gravity = 2200 -- pixels/second^2
local jumpSpeed = -700 -- pixels/second
local startingYPosition = 100
local xPosition = 100

function Player:new()
  self.x = xPosition
  self.y = startingYPosition
  self.width = Player.size
  self.height = Player.size
  self.velocity = 0
end

function Player:update(dt)
  self.velocity = self.velocity + gravity * dt
  self.y = self.y + self.velocity * dt
end

function Player:draw()
  love.graphics.setColor({44, 58, 71})
  love.graphics.rectangle('fill', self.x, self.y, self.width, self.height)
end

function Player:jump()
  self.velocity = jumpSpeed
end