Wall = Object:extend()

Wall.width = 100
Wall.xInterval = 300

local bufferSize = 100
local windowSize = 200
local speed = 400

local colors = {
  {71, 71, 135},
  {34, 112, 147},
  {33, 140, 116},
  {179, 57, 57},
  {255, 121, 63}
}

function Wall:new()
  self.x = WINDOW_WIDTH
  self.windowUpperBound = math.random(bufferSize, WINDOW_HEIGHT - bufferSize * 2)
  self.windowLowerBound = self.windowUpperBound + windowSize
  
  self.upperBlock = Block(self.x, 0, Wall.width, self.windowUpperBound)
  self.lowerBlock = Block(self.x, self.windowLowerBound, Wall.width, WINDOW_HEIGHT)

  self.color = colors[math.random(#colors)]
end

function Wall:update(dt)
  self.x = self.x - speed * dt
  self.upperBlock:update(self.x)
  self.lowerBlock:update(self.x)
end

function Wall:draw()
  love.graphics.setColor(self.color)
  self.upperBlock:draw()
  self.lowerBlock:draw()
end

function Wall:checkCollision(player)
  local player_left = player.x
  local player_right = player.x + player.width
  local player_top = player.y
  local player_bottom = player.y + player.height
  
  local walls_left = self.x
  local walls_right = self.x + Wall.width
  local walls_top = self.upperBlock.height
  local walls_bottom = self.lowerBlock.y
  
  if (player_right > walls_left and
      player_left < walls_right and
      ((player_top < walls_top and player_bottom < walls_bottom)
       or (player_top > walls_top and player_bottom > walls_bottom))) then
    return true
  else
    return false
  end
end